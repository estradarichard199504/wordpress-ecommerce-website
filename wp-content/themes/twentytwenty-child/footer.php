<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

	<div class="wp-block-columns footer-area">
		<div class="wp-block-column footer-col"> 
			<?php if (is_active_sidebar( 'footer-1' ) ) : dynamic_sidebar( 'footer-1' ); endif; ?>
		</div>
		<div class="wp-block-column footer-col">
			<?php if (is_active_sidebar( 'footer-2' ) ) : dynamic_sidebar( 'footer-2' ); endif; ?>
		</div>
		<div class="wp-block-column footer-col">
			<?php if (is_active_sidebar( 'footer-3' ) ) : dynamic_sidebar( 'footer-3' ); endif; ?>
		</div>
		<div class="wp-block-column footer-col">
			<?php if (is_active_sidebar( 'footer-4' ) ) : dynamic_sidebar( 'footer-4' ); endif; ?>
		</div>
	</div>

			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

				<nav class="footer-menu-wrapper">
						<ul class="reset-list-style">
							<?php

							wp_nav_menu( array( 
								'container'  => '',
								'items_wrap' => '%3$s',
								'theme_location' => 'footer-menu' )
							);	
							?>
						</ul>
					</nav>


					<div class="footer-credits">

						<p class="footer-copyright">&copy;
							<?php
							echo date_i18n(
								_x( 'Y', 'copyright date format', 'twentytwenty' )
							);
							?>
							<?php bloginfo( 'name' ); ?>
						</p><!-- .footer-copyright -->


					</div><!-- .footer-credits -->

					<a class="to-the-top" href="#site-header">
						<span class="to-the-top-long">
							<?php
							/* translators: %s: HTML character for up arrow. */
							printf( __( '%s', 'twentytwenty' ), '<i class="fas fa-arrow-up"></i>' );
							?>
						</span><!-- .to-the-top-long -->
						<span class="to-the-top-short">
							<?php
							/* translators: %s: HTML character for up arrow. */
							printf( __( '%s', 'twentytwenty' ), '' );
							?>
						</span><!-- .to-the-top-short -->
					</a><!-- .to-the-top -->

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

	</body>
</html>
