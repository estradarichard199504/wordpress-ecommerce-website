<?php
/**
* Template Name: Home 
*/
get_header();
?>

<main id="site-content" role="main">

<?php

if ( have_posts() ) {

    while ( have_posts() ) {
        the_post();

        get_template_part( 'template-parts/home', get_post_type() );
    }
}

?>

</main><!-- #site-content -->

<?php get_footer(); ?>